package org.udg.pds.simpleapp_android.util;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

/**
 * Created with IntelliJ IDEA.
 * User: imartin
 * Date: 30/03/13
 * Time: 10:47
 * To change this template use File | Settings | File Templates.
 */

// Global variables (yes, I'm evil)
public class Global {
    public static final String tz = TimeZone.getDefault().getDisplayName();
    public static final SimpleDateFormat DATE_ONLY_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
    public static final SimpleDateFormat TIME_ONLY_FORMAT = new SimpleDateFormat("HH::mm");
    public static final SimpleDateFormat TIME_DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy HH::mm");
    public static final SimpleDateFormat FULL_TIME_DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy HH::mm z");

    // Resquest codes for startActivityForResult
    public static final int RQ_ADD_TASK = 1;

    // IMPORTANT: you have to change the value of BASE_URL when deploying the app
    // public static final String BASE_URL = "https://project2-pdsudg.rhcloud.com";
    public static final String BASE_URL = "http://10.0.2.2:8080";


}
