package org.udg.pds.simpleapp_android.xml;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created with IntelliJ IDEA.
 * User: imartin
 * Date: 28/03/13
 * Time: 18:58
 * To change this template use File | Settings | File Templates.
 */
@XStreamAlias("error")
public class XMLError {
    public String msg;
}
