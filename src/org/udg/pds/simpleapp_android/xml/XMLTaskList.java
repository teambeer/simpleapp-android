package org.udg.pds.simpleapp_android.xml;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("collection")
public class XMLTaskList {
	
	@XStreamImplicit
	List<XMLTask> tasks;

	public List<XMLTask> getTaskList() {
		return tasks;
	}

    // IMPORTANT: declaring this method we allow xml strings like "<collection/>" to be
    // translated to a XMLTaksList with an empty list in the "tasks" field.
    // Otherwise we would get a null from the unmarshaling
    private Object readResolve() {
        if(tasks == null){
            tasks = new ArrayList<XMLTask>();
        }
        return this;
    }


    @XStreamAlias("task")
	public static class XMLTask {
		public long id;
		public Date dateCreated;
        public Date dateLimit;
        public Boolean completed;
        public String text;
        public long userId;

        @Override
		public String toString() {
			return id + " -> " + text;
		}
	}
}

