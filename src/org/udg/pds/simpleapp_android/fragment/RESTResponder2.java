package org.udg.pds.simpleapp_android.fragment;

/**
 * Created with IntelliJ IDEA.
 * User: imartin
 * Date: 30/03/13
 * Time: 10:00
 * To change this template use File | Settings | File Templates.
 */

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import com.thoughtworks.xstream.XStream;
import org.udg.pds.simpleapp_android.service.RESTService;
import org.udg.pds.simpleapp_android.util.Global;
import org.udg.pds.simpleapp_android.xml.XMLError;
import org.udg.pds.simpleapp_android.xml.XMLTaskList;

import java.util.Date;

/*
 This fragment contains part of the functionality that we can ask to the server. This fragment takes care of:
 - Add tasks

 We use a Fragment instead of doing it in an Activity because if the REST call completes but our Activity is gone
 our app it will crash. This can happen in several circumstances, like switching from landscape to portrait.
 If there is a call being processed during the time the Activity is being recreated the app will likely crash.

  */

public class RESTResponder2 extends Fragment {

    // We will use this to call the container activity on response from REST calls
    private onRESTResponderResponse mCallback;

    // This is the public interface that the container activity has to implement
    // in order to use this fragment
    public interface onRESTResponderResponse {
        public void onTaskSaved(XMLTaskList.XMLTask t);
        public void onError(XMLError e);
    }

    private static String TAG = RESTResponder2.class.getName();

    private XStream mXstream = new XStream();

    public RESTResponder2() {

        // Initialize XStream object to parse our XML responses from the server
        mXstream.processAnnotations(XMLError.class);
        mXstream.processAnnotations(XMLTaskList.XMLTask.class);
    }

    @Override
    public void onAttach(Activity a) {
        super.onAttach(a);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (onRESTResponderResponse) a;
        } catch (ClassCastException e) {
            throw new ClassCastException(a.toString()
                    + " must implement onRESTResponderListener");
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // This tells our Activity to keep the same instance of this
        // Fragment when the Activity is re-created during lifecycle
        // events. This is what we want because this Fragment should
        // be available to receive results from our RESTService no
        // matter what the Activity is doing.
        setRetainInstance(true);
    }

    // This is the function used to add a new task to the server
    public void addTask(Date dateCreated, Date dateLimit, String text) {
        Activity a = getActivity();
        Intent intent = new Intent(a, RESTService.class);

        // Build the URL with the appropriate resource path
        intent.setData(Uri.parse(Global.BASE_URL + "/rest/task/add"));
        // Set the HTTP verb
        intent.putExtra(RESTService.EXTRA_HTTP_VERB, RESTService.POST);

        // Send the parameters for the POST request in a Bundle
        Bundle params = new Bundle();
        params.putString("dateCreated", Global.FULL_TIME_DATE_FORMAT.format(dateCreated));
        params.putString("dateLimit", Global.FULL_TIME_DATE_FORMAT.format(dateLimit));
        params.putString("text", text);
        intent.putExtra(RESTService.EXTRA_PARAMS, params);

        // Put a receiver in the intent to process the results
        intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, new ResultReceiver(new Handler()) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                if (resultCode == 200) {
                    String xml = resultData.getString(RESTService.REST_RESULT);
                    try {
                        XMLTaskList.XMLTask t = (XMLTaskList.XMLTask) mXstream.fromXML(xml);
                        mCallback.onTaskSaved(t);
                    } catch (Exception ex) {
                        // There has been an error
                        XMLError e = (XMLError) mXstream.fromXML(xml);
                        mCallback.onError(e);
                    }
                }
            }
        });

        // Here we send our Intent to our RESTService.
        a.startService(intent);
    }
}