package org.udg.pds.simpleapp_android.fragment;


import com.thoughtworks.xstream.XStream;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;

import org.udg.pds.simpleapp_android.activity.MainActivity;
import org.udg.pds.simpleapp_android.util.Global;
import org.udg.pds.simpleapp_android.xml.XMLError;
import org.udg.pds.simpleapp_android.xml.XMLTaskList;
import org.udg.pds.simpleapp_android.service.RESTService;
import org.udg.pds.simpleapp_android.xml.XMLUser;

import java.util.Date;

/*
 This fragment contains part of the functionality that we can ask to the server. It is responsible to call the
 REST Service for the authentication, and to update the UI with the received information.

 We use a Fragment instead of doing it in an Activity because if the REST call completes but our Activity is gone
 our app it will crash. This can happen in several circumstances, like switching from landscape to portrait.
 If there is a call being processed during the time the Activity is being recreated the app will likely crash.

  */

public class RESTResponder extends Fragment {

    // We will use this to call the container activity on response from REST calls
    private onRESTResponderListener mCallback;

    // This is the public interface that the container activity has to implement
    // in order to use this fragment
    public interface onRESTResponderListener {
        public void onAuthSuccessful(XMLUser user);
        public void onError(XMLError e);
        public void onTaskListUpdate(XMLTaskList tl);
    }

    private static String TAG = RESTResponder.class.getName();

    // This is the XStream objet that we will use to comvert XML responses into objects
    private XStream mXstream = new XStream();

    private XMLTaskList mTasks; // It contains the data of the current tasks that we are showing

    public RESTResponder() {

        // Initalize XStream object to parse our XML responses from the server
        mXstream.processAnnotations(XMLError.class);
        mXstream.processAnnotations(XMLUser.class);
        mXstream.processAnnotations(XMLTaskList.class);
        mXstream.processAnnotations(XMLTaskList.XMLTask.class);
    }

    @Override
    public void onAttach(Activity a) {
        super.onAttach(a);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (onRESTResponderListener) a;
        } catch (ClassCastException e) {
            throw new ClassCastException(a.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // This tells our Activity to keep the same instance of this
        // Fragment when the Activity is re-created during lifecycle
        // events. This is what we want because this Fragment should
        // be available to receive results from our RESTService no
        // matter what the Activity is doing.
        setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    public void doAuthentication(String username, String password) {
        Activity a = getActivity();
        Intent intent = new Intent(a, RESTService.class);

        // Build the URL with the appropriate resource path
        intent.setData(Uri.parse(Global.BASE_URL + "/rest/auth"));
        // Set the HTTP verb
        intent.putExtra(RESTService.EXTRA_HTTP_VERB, RESTService.POST);

        // Send the parameters for the POST request in a Bundle
        Bundle params = new Bundle();
        params.putString("username", username);
        params.putString("password", password);
        intent.putExtra(RESTService.EXTRA_PARAMS, params);

        // Put a receiver in the intent to process the results
        intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, new ResultReceiver(new Handler()) {
            // This function will be called from the REST Service when it invokes the
            // send() command of the ResultReceiver
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                if (resultCode == 200) {
                    String xml = resultData.getString(RESTService.REST_RESULT);
                    try {
                        XMLUser user = (XMLUser) mXstream.fromXML(xml);
                        mCallback.onAuthSuccessful(user);
                    } catch (Exception ex) {
                        // There has been an error
                        XMLError e = (XMLError) mXstream.fromXML(xml);
                        mCallback.onError(e);
                    }
                }
            }
        });

        // Here we send our Intent to our RESTService.
        a.startService(intent);

    }

    // This is the function used to retrieve all the tasks from an user from the server
    public void getTasks() {
        Activity activity =  getActivity();

        if (mTasks == null && activity != null) {
            // This is where we make our REST call to the service. We also pass in our ResultReceiver
            // defined in the RESTResponderFragment super class.

            // We will explicitly call our Service since we probably want to keep it as a private
            // component in our app. You could do this with Intent actions as well, but you have
            // to make sure you define your intent filters correctly in your manifest.
            Intent intent = new Intent(activity, RESTService.class);

            // Set the resource URL
            intent.setData(Uri.parse(Global.BASE_URL + "/rest/task/all"));
            // Set the HTTP verb
            intent.putExtra(RESTService.EXTRA_HTTP_VERB, RESTService.GET);

            // Finally we put a ResultReceiver into the intent, that the RESTService
            // will use to send back the results
            intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, new ResultReceiver(new Handler()) {
                @Override
                protected void onReceiveResult(int resultCode, Bundle resultData) {
                    if (resultCode == 200) {
                        String xml = resultData.getString(RESTService.REST_RESULT);
                        try {
                            XMLTaskList tl = (XMLTaskList) mXstream.fromXML(xml);
                            mCallback.onTaskListUpdate(tl);
                        } catch (Exception ex) {
                            // There has been an error
                            XMLError e = (XMLError) mXstream.fromXML(xml);
                            mCallback.onError(e);
                        }
                    }
                }});

            // Here we send our Intent to our RESTService.
            activity.startService(intent);
        }
    }
}