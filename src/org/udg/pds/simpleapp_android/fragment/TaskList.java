package org.udg.pds.simpleapp_android.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import org.udg.pds.simpleapp_android.R;
import org.udg.pds.simpleapp_android.activity.AddTask;
import org.udg.pds.simpleapp_android.adapter.TasksAdapter;
import org.udg.pds.simpleapp_android.util.Global;
import org.udg.pds.simpleapp_android.xml.XMLTaskList;

/**
 * Created with IntelliJ IDEA.
 * User: imartin
 * Date: 28/03/13
 * Time: 15:49
 * To change this template use File | Settings | File Templates.
 */


/**
 * This Fragment contains the Tasks list layout
 */
public class TaskList extends ListFragment {

    // We will use this to call the container activity on response from REST calls
    private OnTaskListListener mCallback;

    // This is the interface that the container activity has to implement
    // in order to use this fragment
    public interface OnTaskListListener {
        public void updateTaskList();
    }

    // This is the adapter responsible to show the list
    private TasksAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.task_list, container, false);
        Button b = (Button) view.findViewById(R.id.add_task_button);
        // This is the listener to the "Add Task" button
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // When we press the "Add Task" button, the AddTask activity is called, where
                // we can introduce the data of the new task
                Intent i = new Intent(getActivity(), AddTask.class);
                // We launch the activity with startActivityForResult because we want to know when
                // the launched activity has finished. In this case, when the AddTask activity has finished
                // we will update the list to show the new task.
                startActivityForResult(i, Global.RQ_ADD_TASK);
            }
        });
        return view;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // This tells our Activity to keep the same instance of this
        // Fragment when the Activity is re-created during lifecycle
        // events.
        setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Since onActivityCreated can be called multiple times in the Fragment lifecycle, we only initialize the
        // adapter if it is null (the first time)
        if (mAdapter == null) {
            mAdapter = new TasksAdapter(getActivity(), R.layout.task_layout);
            setListAdapter(mAdapter);
        }
    }

    @Override
    public void onAttach(Activity a) {
        super.onAttach(a);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnTaskListListener) a;
        } catch (ClassCastException e) {
            throw new ClassCastException(a.toString()
                    + " must implement OnTaskListListener");
        }
    }

    // This function is called from the container activity whenever it has received
    // a new task list from a REST responder
    public void showTaskList(XMLTaskList tl) {
        // Load our list adapter with our Tasks. That would cause and automatic update
        // of the corresponding ListView
        mAdapter.clear();
        for (XMLTaskList.XMLTask t : tl.getTaskList()) {
            mAdapter.add(t);
        }
    }

    // This function is called whenever an Activity launched with startActivityForResult
    // finishes
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data ) {
          if (requestCode == Global.RQ_ADD_TASK) {
            mCallback.updateTaskList();
          }


    }
}