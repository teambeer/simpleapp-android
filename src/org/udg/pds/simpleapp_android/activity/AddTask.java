package org.udg.pds.simpleapp_android.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.ResultReceiver;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import org.udg.pds.simpleapp_android.R;
import org.udg.pds.simpleapp_android.fragment.RESTResponder2;
import org.udg.pds.simpleapp_android.util.Global;
import org.udg.pds.simpleapp_android.xml.XMLError;
import org.udg.pds.simpleapp_android.xml.XMLTaskList;

import java.util.Calendar;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: imartin
 * Date: 29/03/13
 * Time: 0:45
 * To change this template use File | Settings | File Templates.
 */

// Fragment used to create a new task
public class AddTask extends FragmentActivity implements RESTResponder2.onRESTResponderResponse {


    // We will use a Handler to return time from the time selection dialog to the Activity
    Handler mHandlerT = new Handler(){
        @Override
        public void handleMessage(Message msg){
            Bundle b = msg.getData();
            Integer hour = b.getInt("hour");
            Integer minute = b.getInt("minute");
            TextView tl = (TextView) findViewById(R.id.at_time_limit);
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR, hour);
            cal.set(Calendar.MINUTE, minute);
            tl.setText(Global.TIME_ONLY_FORMAT.format(cal.getTime()));
        }
    };

    // We will use a Handler to return date from the date selection dialog to the Activity
    Handler mHandlerD = new Handler(){
        @Override
        public void handleMessage(Message msg){
            Bundle b = msg.getData();
            Integer day = b.getInt("day");
            Integer month = b.getInt("month");
            Integer year = b.getInt("year");

            TextView tl = (TextView) findViewById(R.id.at_date_limit);
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.DAY_OF_MONTH, day);
            cal.set(Calendar.MONTH, month);
            cal.set(Calendar.YEAR, year);
            tl.setText(Global.DATE_ONLY_FORMAT.format(cal.getTime()));
        }
    };

    // This class is a Dialog used by the user to introduce a time (HH::mm)
    // It is shown when the user presses the "Set" button
    // in the "time limit" field
    private static class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

        Handler mH;

        public TimePickerFragment(Handler h) {
            mH = h;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            Message msg = new Message();
            Bundle data = new Bundle();
            data.putInt("hour", hourOfDay);
            data.putInt("minute", minute);
            msg.setData(data);
            mH.sendMessage(msg);
        }
    }

    // This class is a Dialog user by the user to introduce a time (dd/mm/yyyy)
    // It is shown when the user presses the "Set" button
    // in the "date limit" field
    private static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        Handler mH;

        public DatePickerFragment(Handler h) {
            mH = h;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int day = c.get(Calendar.DAY_OF_MONTH);
            int month = c.get(Calendar.MONTH);
            int year = c.get(Calendar.YEAR);

            // Create a new instance of TimePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            Message msg = new Message();
            Bundle data = new Bundle();
            data.putInt("day", day);
            data.putInt("month", month);
            data.putInt("year", year);

            msg.setData(data);
            mH.sendMessage(msg);
        }
    }

    // This is the REST responder that this fragment will use to perform the REST calls
    // Remember that all the calls to the REST Service have to be made from a Fragment
    // because the Activity can be destroyed with a configuration change in the middle of a call
    private RESTResponder2 mResponder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_add_task);

        FragmentManager fm = getSupportFragmentManager();

        // First we check if the responder has been already created. If not, initialize
        mResponder = (RESTResponder2) fm.findFragmentByTag("RESTResponder2");
        if (mResponder == null) {
            FragmentTransaction ft = fm.beginTransaction();
            mResponder = new RESTResponder2();
            // We add the fragment using a Tag since it has no views.
            ft.add(mResponder, "RESTResponder2");
            // Make sure you commit the FragmentTransaction or your fragments
            // won't get added to your FragmentManager. Forgetting to call ft.commit()
            // is a really common mistake when starting out with Fragments.
            ft.commit();
        }

        Button timeButton = (Button) findViewById(R.id.at_time_limit_button);
        // Show the time selection dialog when the "Set" button is pressed
        timeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerFragment dialog = new TimePickerFragment(mHandlerT);
                dialog.show(getSupportFragmentManager(), "timepickerdialog");
            }
        });

        Button dateButton = (Button) findViewById(R.id.at_date_limit_button);
        // Show the date selection dialog when the "Set" button is pressed
        dateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerFragment dialog = new DatePickerFragment(mHandlerD);
                dialog.show(getSupportFragmentManager(), "timepickerdialog");
            }
        });

        Button save = (Button) findViewById(R.id.at_save_button);
        // When the "Save" button is pressed, we make the call to the responder
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView datev = (TextView) findViewById(R.id.at_date_limit);
                TextView timev = (TextView) findViewById(R.id.at_time_limit);
                TextView textv = (TextView) findViewById(R.id.at_text);

                try {
                    Date dateLimit = Global.TIME_DATE_FORMAT.parse(datev.getText().toString() + " " + timev.getText().toString());
                    String text = textv.getText().toString();
                    mResponder.addTask(new Date(), dateLimit, text);
                } catch (Exception ex) {
                    return;
                }

            }
        });

    }

    // This function is called from RESTResponder2 when a task has successfully added
    // The only thing to do is to finish the activity and return back
    @Override
    public void onTaskSaved(XMLTaskList.XMLTask t) {
        finish();
    }

    // This function is called from RESTResponder2 when there has been an error adding a task
    // We show a dialog with the error message, and when the user pers "Ok" we finish the task
    // and go back
    @Override
    public void onError(XMLError e) {
        org.udg.pds.simpleapp_android.util.Dialog.onError("Error", this, e, new ResultReceiver(new Handler()) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                finish();
            }
        });
    }
}