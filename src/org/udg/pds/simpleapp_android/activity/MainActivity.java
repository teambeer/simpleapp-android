package org.udg.pds.simpleapp_android.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import org.udg.pds.simpleapp_android.R;
import org.udg.pds.simpleapp_android.fragment.Login;
import org.udg.pds.simpleapp_android.fragment.RESTResponder;
import org.udg.pds.simpleapp_android.fragment.TaskList;
import org.udg.pds.simpleapp_android.util.Dialog;
import org.udg.pds.simpleapp_android.xml.XMLError;
import org.udg.pds.simpleapp_android.xml.XMLTaskList;
import org.udg.pds.simpleapp_android.xml.XMLUser;

// FragmentActivity is a base class for activities that want to use the support-based Fragment and Loader APIs.
// http://developer.android.com/reference/android/support/v4/app/FragmentActivity.html
public class MainActivity extends FragmentActivity
        implements
        Login.OnLoginListener,
        RESTResponder.onRESTResponderListener,
        TaskList.OnTaskListListener {

    // This is the REST responder that this fragment will use to perform the REST calls
    // Remember that all the calls to the REST Service have to be made from a Fragment
    // because the Activity can be destroyed with a configuration change in the middle of a call
    private RESTResponder mResponder;
    private TaskList mTaskList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_main);

        FragmentManager     fm = getSupportFragmentManager();

        // First we check if the responder has been already created. If not, initialize
        mResponder = (RESTResponder) fm.findFragmentByTag("RESTResponder");
        if (mResponder == null) {
            FragmentTransaction ft = fm.beginTransaction();
            mResponder = new RESTResponder();
            // We add the fragment using a Tag since it has no views.
            ft.add(mResponder, "RESTResponder");
            // Make sure you commit the FragmentTransaction or your fragments
            // won't get added to your FragmentManager. Forgetting to call ft.commit()
            // is a really common mistake when starting out with Fragments.
            ft.commit();
        }

        // Check if the TaskList fragment exists. If not it means that the user has not authenticated
        // show the login fragment
        TaskList tl = (TaskList) fm.findFragmentByTag("TaskList");
        if (tl == null) {
            login();
            return;
        }

        // If the fragment exists, add it to the activity
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.fragment_content, tl, "TaskList");
        ft.commit();

    }

    /**
     * Shows the login fragment
     */
    void login() {
        FragmentManager     fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Login l = new Login();
        ft.add(R.id.fragment_content, l);
        ft.commit();
    }

    // This method is called when the "Login" button is pressed in the Login fragment
    @Override
    public void checkCredentials(String username, String password) {
        mResponder.doAuthentication(username, password);
    }

    // This method is called from RESTResponder if the authenticantion has been successful
    @Override
    public void onAuthSuccessful(XMLUser u) {
        // Once the user has authenticated we can remove the login fragment and add the TaskList one
        FragmentManager     fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        mTaskList =  new TaskList();
        ft.replace(R.id.fragment_content, mTaskList, "TaskList");
        ft.commit();
        this.updateTaskList();
    }

    // This function is called whenever we want to update the task list
    // It just calls the responder to make the REST call
    @Override
    public void updateTaskList() {
        mResponder.getTasks();
    }

    // This function is called from RESTResponder whenever the task list needs to be updated
    // This function is just the communication point between the RESTResponder fragment and the
    // TaskList fragment. Remember that the way that Fragments communicate it is through
    // the activity to which they are attached
    @Override
    public void onTaskListUpdate(XMLTaskList tl) {
        mTaskList.showTaskList(tl);
    }


    // This method is called from RESTResponder if any server operation had an error
    @Override
    public void onError(XMLError e) {
        Dialog.onError("Error", this, e,  new ResultReceiver(new Handler()) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
            }});
    }
}